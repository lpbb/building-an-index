
import json
from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize
import requests

class WebIndexer:
    def __init__(self, urls_file) -> None:
        self.urls = json.load(open(urls_file))
        self.titles = []
        self.tokens = []
        self.index = {}
        self.num_docs = 0
        self.num_tokens = 0
        self.avg_tokens_per_doc = 0

    def extract_titles(self) -> None:
        for url in self.urls:
            try:
                response = requests.get(url, timeout= 5)
            except:
                continue
            soup = BeautifulSoup(response.text, "html.parser")
            try:
                self.titles.append(soup.title.string)
            except:
                continue

    def tokenize(self) -> None:
        self.tokens = [word_tokenize(str(title)) for title in self.titles]

    def build_index(self):
        for i, doc_tokens in enumerate(self.tokens):
            for token in doc_tokens:
                if token not in self.index:
                    self.index[token] = {}
                if i not in self.index[token]:
                    self.index[token][i] = 0
                self.index[token][i] += 1


    def calculate_stats(self) -> None:
        self.num_docs = len(self.titles)
        self.num_tokens = sum([len(doc) for doc in self.tokens])
        self.avg_tokens_per_doc = self.num_tokens / self.num_docs

    def save_index(self, index_file) -> None:
        json.dump(self.index, open(index_file, "w"))

    def save_stats(self, stats_file) -> None:
        stats = {"num_docs": self.num_docs, "num_tokens": self.num_tokens, "avg_tokens_per_doc": self.avg_tokens_per_doc}
        json.dump(stats, open(stats_file, "w"))

    def process(self) -> None:
        self.extract_titles()
        self.tokenize()
        self.build_index()
        self.calculate_stats()


