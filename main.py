from Index.web_indexer import WebIndexer
import argparse



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("crawled_urls_path", help="the PATH of the file containing the urls that have been crawled")
    args = parser.parse_args()
    indexer = WebIndexer(args.crawled_urls_path)
    indexer.process()
    indexer.save_index("title.non_pos_index.json")
    indexer.save_stats("metadata.json")
