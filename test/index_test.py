from Index.web_indexer import WebIndexer
import os 
import unittest
import json

class TestWebIndexer(unittest.TestCase):
    def setUp(self) -> None:
        self.urls_file = "urls.json"
        self.index_file = "index.json"
        self.stats_file = "stats.json"
        self.urls = ['https://www.google.com', 'https://www.facebook.com']
        json.dump(self.urls, open(self.urls_file, "w"))
        self.web_indexer = WebIndexer(self.urls_file)

    def test_process(self):
        self.web_indexer.process()
        self.assertEqual(len(self.web_indexer.titles), 2)
        self.assertEqual(len(self.web_indexer.tokens), 2)
        self.assertGreater(len(self.web_indexer.index), 0)
        self.assertGreater(self.web_indexer.num_docs, 0)
        self.assertGreater(self.web_indexer.num_tokens, 0)
        self.assertGreater(self.web_indexer.avg_tokens_per_doc, 0)

    def test_save_index(self):
        self.web_indexer.save_index(self.index_file)
        self.assertTrue(json.load(open(self.index_file))== {})
        os.remove("index.json")

    def test_save_stats(self):
        self.web_indexer.save_stats(self.stats_file)
        self.assertTrue(json.load(open(self.stats_file)))
        os.remove("stats.json")

    def tearDown(self) -> None:
        # Delete the test files after the tests are done
        open(self.urls_file, "w").close()
        open(self.index_file, "w").close()
        open(self.stats_file, "w").close()
        os.remove("index.json")
        os.remove("stats.json")
        os.remove("urls.json")

