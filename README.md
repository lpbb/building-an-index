# Building an Index

Crée un index non positionel à partir d'un fichier d'url en utilisant les titres.

## Install

```
git clone https://gitlab.com/lpbb/building-an-index.git
cd building-an-index
pip install -r requirements.txt
```

## Launch

```
python3 main.py path_to_url_file
```

## Return

`title.non_pos_index.json` comprenant l'index.  
`metadata.json` comprenant des statistiques sur les documents.

## Tests

Pour lancer les tests :

```
python3 -m unittest test/test.py
```

## Difficulties

Il y a un message de warning lors du lancement des tests. Les tests sont fonctionnels mais  
il y a un problème de gestion de mémoire, les fichiers créés pour l'occasion ne sont pas fermés.

## Author

Perraud Louis
